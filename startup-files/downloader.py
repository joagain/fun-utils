import logging
import yaml
import pathlib
from pathlib import Path
from array import array
import sys

class Config:

     def __init__(self, cfgd: dict):
        self.url = cfgd.get('url')
        self.file = cfgd.get('file_location')
        self.local_files = cfgd.get('custom_files') # array


def load_config(config_file: str):
    log = logging.getLogger('load_config')
    log.info("Loading from " + config_file)
    working_directory = pathlib.PosixPath('.').resolve()
    f = working_directory / config_file
    if not Path(f).is_file():
        return None
    with open(f, 'r') as file:
        config = yaml.load(file)
        log.info("Loaded config")
        return config


def download(url: str):
    log = logging.getLogger('download')
    import requests
    r = requests.get(url)
    log.info("Downloaded contents from " + url)
    import tempfile
    with tempfile.NamedTemporaryFile(delete=False) as temp:
        temp.write(r.content)
        temp.flush()
    log.info("Wrote to temporary file " + temp.name)
    return temp.name


def append(file: str, local_files: array):
    log = logging.getLogger('append')
    if local_files:
        with open(file, 'w') as f:
            for local_file in local_files:
                # TODO: only works if each of the local_file fits in memory
                log.info("Appending contents from " + local_file)
                if not Path(local_file).is_file():
                    log.warn(local_file + " does not exist and has been skipped")
                    continue
                f.write(local_file.read())


def color_diff(diff):
    from colorama import Fore, Back, Style, init
    for line in diff:
        if line.startswith('+'):
            yield Fore.GREEN + line + Fore.RESET
        elif line.startswith('-'):
            yield Fore.RED + line + Fore.RESET
        elif line.startswith('^'):
            yield Fore.BLUE + line + Fore.RESET
        else:
            yield line


def compare(old_file: str, new_file: str):
    import difflib
    # returns a delta (a generator generating the delta lines)
    log = logging.getLogger('compare')
    if not Path(old_file).is_file():
        log.info("There is no existing script to compare with")
        return None
    log.info("Comparing " + old_file + " with " + new_file)
    diff = difflib.unified_diff(
        open(old_file).readlines(),
        open(new_file).readlines(),
        fromfile=old_file,
        tofile=new_file)
    if all(False for _ in diff):
        print("Your current script is already up-to-date.")
        return None
    sys.stdout.writelines(color_diff(diff))
    return diff


def yes_no(question):
    yes = set(['yes','y', 'ye'])
    no = set(['no','n'])
     
    while True:
        choice = input(question).lower()
        if choice in yes:
           return True
        elif choice in no:
           return False
        else:
           print("Please respond with 'yes' or 'no'")


def move(old_file: str, new_file:str):
    log = logging.getLogger('replace')
    log.info("Moving " + old_file + " with " + new_file)
    import shutil
    shutil.move(new_file, old_file)


if __name__ == "__main__":
    log = logging.getLogger('main')
    logging.basicConfig(level='INFO', stream=sys.stdout)

    # TODO: remove hardcode
    dl_config = load_config('downloader_config.yml')

    if not dl_config:
        log.error("Yaml config cannot be empty.")
        sys.exit(1)

    for key, val in dl_config.items():
        log.info("Parsing \'" + key + "\' configuration")
        config = Config(val)
        dled_file = download(config.url)
        append(dled_file, config.local_files)
        diff = compare(config.file, dled_file)
        if diff:
            if yes_no('Do you wish to proceed and overwrite the existing file? '):
                move(config.file, dled_file)
            else:
                log.info("Not overwriting current script")
        elif not Path(config.file).is_file():
            log.info("Writing downloaded file into a brand new file")
            move(config.file, dled_file)
    log.info("Done")

    # TODO: if we fail/throw an exception/choose not to overwrite the current file,
    # clean up temporary files that might have gotten created